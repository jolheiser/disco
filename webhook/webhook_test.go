//+build e2e

package webhook

import (
	"context"
	"io"
	"os"
	"testing"

	"go.jolheiser.com/disco/embed"
)

func TestMain(m *testing.M) {
	os.Exit(m.Run())
}

func TestWebhook(t *testing.T) {
	u := os.Getenv("DISCO_WEBHOOK")
	if u == "" {
		return
	}

	webhook := Webhook{
		Content:   "@everyone Webhook Content! (No Ping)",
		Username:  "Disco",
		AvatarURL: "https://gitea.com/user/avatar/jolheiser/-1",
		Embeds: []*embed.Embed{
			{
				URL:         "https://gitea.com/jolheiser/disco",
				Type:        embed.TypeRich,
				Title:       "Disco Webhook",
				Description: "A webhook created with disco",
				Timestamp:   embed.Now(),
				Color:       0x007D96,
				Author: &embed.Author{
					Name:    "jolheiser",
					URL:     "https://gitea.com/jolheiser",
					IconURL: "https://gitea.com/user/avatar/jolheiser/-1",
				},
				Footer: &embed.Footer{
					Text:    "Webhook Footer",
					IconURL: "https://gitea.com/user/avatar/jolheiser/-1",
				},
				Image: &embed.Image{
					URL: "https://gitea.com/user/avatar/jolheiser/-1",
				},
				Fields: []*embed.Field{
					{
						Name:   "Library",
						Value:  "Disco",
						Inline: true,
					},
					{
						Name:   "Usage",
						Value:  "Discord embeds/webhooks",
						Inline: true,
					},
				},
			},
		},
		AllowedMentions: &embed.AllowedMentions{
			Parse: []embed.Parse{embed.Users},
		},
	}

	resp, err := webhook.Send(context.Background(), u)
	if err != nil {
		t.Log(err)
		t.FailNow()
	}

	if resp.StatusCode != 204 {
		respText, _ := io.ReadAll(resp.Body)
		defer resp.Body.Close()
		t.Logf("Non-204 status returned: %d\n%s", resp.StatusCode, respText)
		t.FailNow()
	}
}
