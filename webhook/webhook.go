package webhook

import (
	"bytes"
	"context"
	"encoding/json"
	"net/http"

	"go.jolheiser.com/disco/embed"
)

// Webhook is a Discord webhook
type Webhook struct {
	Content         string                 `json:"content"`
	Username        string                 `json:"username,omitempty"`
	AvatarURL       string                 `json:"avatar_url,omitempty"`
	TTS             bool                   `json:"tts,omitempty"`
	Embeds          []*embed.Embed         `json:"embeds"`
	AllowedMentions *embed.AllowedMentions `json:"allowed_mentions"`
}

// WebhookEdit https://discord.com/developers/docs/resources/webhook#edit-webhook-message
type WebhookEdit struct {
	Content         string                 `json:"content"`
	Embeds          []*embed.Embed         `json:"embeds"`
	AllowedMentions *embed.AllowedMentions `json:"allowed_mentions"`
}

// Request returns the Webhook as an *http.Request payload
//
// Useful if using with a custom http.Client
func (w *Webhook) Request(ctx context.Context, webhookURL string) (*http.Request, error) {
	payload, err := json.Marshal(w)
	if err != nil {
		return nil, err
	}
	req, err := http.NewRequestWithContext(ctx, http.MethodPost, webhookURL, bytes.NewBuffer(payload))
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", "application/json")
	return req, nil
}

// Send is a basic convenience for sending a Webhook
func (w *Webhook) Send(ctx context.Context, webhookURL string) (*http.Response, error) {
	req, err := w.Request(ctx, webhookURL)
	if err != nil {
		return nil, err
	}
	return http.DefaultClient.Do(req)
}
