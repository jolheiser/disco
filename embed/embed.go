package embed

import "time"

// Type is the type of embed
type Type string

// Embed Type
const (
	TypeRich    Type = "rich"
	TypeImage   Type = "image"
	TypeVideo   Type = "video"
	TypeGifv    Type = "gifv"
	TypeArticle Type = "article"
	TypeLink    Type = "link"
)

// An Embed is sent in a message or webhook
type Embed struct {
	Title       string     `json:"title,omitempty"`
	Type        Type       `json:"type,omitempty"`
	Description string     `json:"description,omitempty"`
	URL         string     `json:"url,omitempty"`
	Timestamp   *time.Time `json:"timestamp,omitempty"`
	Color       int        `json:"color,omitempty"`
	Footer      *Footer    `json:"footer,omitempty"`
	Image       *Image     `json:"image,omitempty"`
	Thumbnail   *Thumbnail `json:"thumbnail,omitempty"`
	Video       *Video     `json:"video,omitempty"`
	Provider    *Provider  `json:"provider,omitempty"`
	Author      *Author    `json:"author,omitempty"`
	Fields      []*Field   `json:"fields,omitempty"`
}

// Now just wraps time.Now and returns a pointer that works with Embed.Timestamp
func Now() *time.Time {
	now := time.Now()
	return &now
}
