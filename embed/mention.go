package embed

// Parse is the parse types of AllowedMentions
type Parse string

const (
	Roles    Parse = "roles"
	Users    Parse = "users"
	Everyone Parse = "everyone"
)

// AllowedMentions is the allowed mentions to parse in a Webhook
type AllowedMentions struct {
	Parse []Parse  `json:"parse"`
	Roles []string `json:"roles,omitempty"`
	Users []string `json:"users,omitempty"`
}
