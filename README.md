# Disco

A Discord web library. No gateway, just embeds, webhooks, and slash commands.

[example](webhook/webhook_test.go)

## License

[MIT](LICENSE)