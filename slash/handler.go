package slash

import (
	"bytes"
	"crypto/ed25519"
	"encoding/hex"
	"encoding/json"
	"io"
	"net/http"
)

// Handler returns an http.Handler capable of responding to slash commands
func Handler(key string, commands []*Command) (http.Handler, error) {
	hexKey, err := hex.DecodeString(key)
	if err != nil {
		return nil, err
	}
	mux := http.NewServeMux()
	mux.Handle("/", handler(hexKey, commands))
	return mux, nil
}

func handler(hexKey []byte, commands []*Command) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// Verify Signature
		if ok := verifyMiddleware(hexKey, w, r); !ok {
			return
		}

		var interaction *Interaction
		if err := json.NewDecoder(r.Body).Decode(&interaction); err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		// Pre-ACK
		if interaction.Type == 0 {
			http.Error(w, "invalid body", http.StatusBadRequest)
			return
		}

		// ACK
		if interaction.Type == 1 {
			if err := json.NewEncoder(w).Encode(map[string]int{"type": 1}); err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
			}
			return
		}

		// Interaction
		for _, cmd := range commands {
			if cmd.ID == interaction.Data.ID {
				resp, err := cmd.Handle(interaction)
				if err != nil {
					http.Error(w, err.Error(), http.StatusInternalServerError)
					return
				}
				w.Header().Set("Content-Type", "application/json")
				if err := json.NewEncoder(w).Encode(resp); err != nil {
					http.Error(w, err.Error(), http.StatusInternalServerError)
				}
				break
			}
		}
	}
}

func verifyMiddleware(key []byte, w http.ResponseWriter, r *http.Request) bool {
	signature := r.Header.Get("X-Signature-Ed25519")
	timestamp := r.Header.Get("X-Signature-Timestamp")

	sig, err := hex.DecodeString(signature)
	if err != nil {
		http.Error(w, err.Error(), http.StatusUnauthorized)
		return false
	}

	body := new(bytes.Buffer)
	if _, err := io.Copy(body, r.Body); err != nil {
		http.Error(w, err.Error(), http.StatusUnauthorized)
		return false
	}

	buf := bytes.NewBufferString(timestamp + body.String())
	if !ed25519.Verify(key, buf.Bytes(), sig) {
		http.Error(w, "invalid signature", http.StatusUnauthorized)
		return false
	}

	r.Body = io.NopCloser(body)
	return true
}
