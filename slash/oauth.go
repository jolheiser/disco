package slash

import (
	"fmt"
	"time"
)

type oauth struct {
	AccessToken string `json:"access_token"`
	TokenType   string `json:"token_type"`
	ExpiresIn   int    `json:"expires_in"`
	Scope       string `json:"scope"`

	ExpiresAt time.Time
}

func (o oauth) header() string {
	return fmt.Sprintf("%s %s", o.TokenType, o.AccessToken)
}
