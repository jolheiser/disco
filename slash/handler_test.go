package slash

import (
	"crypto/ed25519"
	"encoding/hex"
	"fmt"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
	"time"
)

func TestACK(t *testing.T) {
	pub, priv, err := ed25519.GenerateKey(nil)
	if err != nil {
		t.Log(err)
		t.FailNow()
	}
	_, bad, err := ed25519.GenerateKey(nil)
	if err != nil {
		t.Log(err)
		t.FailNow()
	}
	timestamp := fmt.Sprint(time.Now().Unix())

	tt := []struct {
		Name   string
		Body   string
		Key    ed25519.PrivateKey
		Status int
	}{
		{
			Name:   "Correct",
			Body:   `{"type": 1}`,
			Key:    priv,
			Status: http.StatusOK,
		},
		{
			Name:   "Bad Key",
			Body:   `{"type": 1}`,
			Key:    bad,
			Status: http.StatusUnauthorized,
		},
		{
			Name:   "Blank Body",
			Body:   "",
			Key:    priv,
			Status: http.StatusBadRequest,
		},
	}

	handler, err := Handler(hex.EncodeToString(pub), nil)
	if err != nil {
		t.Log(err)
		t.FailNow()
	}
	s := httptest.NewServer(handler)
	defer s.Close()

	for _, tc := range tt {
		t.Run(tc.Name, func(t *testing.T) {
			req, err := http.NewRequest(http.MethodPost, s.URL, strings.NewReader(tc.Body))
			if err != nil {
				t.Log(err)
				t.FailNow()
			}
			req.Header.Set("X-Signature-Timestamp", timestamp)
			sig := ed25519.Sign(tc.Key, []byte(timestamp+tc.Body))
			req.Header.Set("X-Signature-Ed25519", hex.EncodeToString(sig))

			resp, err := http.DefaultClient.Do(req)
			if err != nil {
				t.Log(err)
				t.FailNow()
			}

			if resp.StatusCode != tc.Status {
				t.Logf("inccorrect response, expected status %d but got %d\n", tc.Status, resp.StatusCode)
				t.FailNow()
			}
		})
	}
}
