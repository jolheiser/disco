package slash

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"strings"
	"time"
)

var baseEndpoint = "https://discord.com/api/v8/"

// Client is a slash client
type Client struct {
	clientID     string
	clientSecret string
	http         *http.Client

	oauth oauth
}

// ClientOption is options for a Client
type ClientOption func(*Client)

// WithHTTP sets the http.Client for a Client
func WithHTTP(client *http.Client) ClientOption {
	return func(c *Client) {
		c.http = client
	}
}

// NewClient creates a new Client for working with slash commands
func NewClient(clientID, clientSecret string, opts ...ClientOption) *Client {
	c := &Client{
		clientID:     clientID,
		clientSecret: clientSecret,
		http:         http.DefaultClient,
	}
	for _, opt := range opts {
		opt(c)
	}
	return c
}

// OAuthURL returns a URL suitable for giving an app slash command scope
func (c *Client) OAuthURL() string {
	return fmt.Sprintf("https://discord.com/api/oauth2/authorize?client_id=%s&scope=applications.commands", c.clientID)
}

func (c *Client) newRequest(ctx context.Context, method, url string, body io.Reader) (*http.Request, error) {
	if err := c.checkToken(ctx); err != nil {
		return nil, err
	}
	req, err := http.NewRequestWithContext(ctx, method, url, body)
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", c.oauth.header())
	return req, nil
}

func (c *Client) checkToken(ctx context.Context) error {
	if time.Now().Before(c.oauth.ExpiresAt) {
		return nil
	}

	data := url.Values{
		"grant_type": []string{"client_credentials"},
		"scope":      []string{"applications.commands.update"},
	}

	endpoint := baseEndpoint + "oauth2/token"
	req, err := http.NewRequestWithContext(ctx, http.MethodPost, endpoint, strings.NewReader(data.Encode()))
	if err != nil {
		return err
	}
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	req.SetBasicAuth(c.clientID, c.clientSecret)
	resp, err := c.http.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return errors.New("could not get client credentials token")
	}

	var oauth oauth
	if err := json.NewDecoder(resp.Body).Decode(&oauth); err != nil {
		return err
	}
	oauth.ExpiresAt = time.Now().Add(time.Second * time.Duration(oauth.ExpiresIn))
	c.oauth = oauth
	return nil
}

func newBuffer(val interface{}) (bytes.Buffer, error) {
	var buf bytes.Buffer
	return buf, json.NewEncoder(&buf).Encode(val)
}

func errMsg(r io.Reader) string {
	msg, err := io.ReadAll(r)
	if err != nil {
		return err.Error()
	}
	return string(msg)
}
