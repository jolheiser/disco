package slash

import (
	"context"
	"fmt"
	"net/http"

	"go.jolheiser.com/disco/webhook"
)

// CreateInteractionResponse creates an interaction response
func (c *Client) CreateInteractionResponse(ctx context.Context, interactionID, interactionToken string, interaction *InteractionResponse) error {
	endpoint := baseEndpoint + fmt.Sprintf("interactions/%s/%s/callback", interactionID, interactionToken)
	buf, err := newBuffer(interaction)
	if err != nil {
		return err
	}
	req, err := c.newRequest(ctx, http.MethodPost, endpoint, &buf)
	if err != nil {
		return err
	}
	_, err = c.http.Do(req)
	return err
}

// EditInteractionResponse edits an interaction response
func (c *Client) EditInteractionResponse(ctx context.Context, interactionToken string, interaction *webhook.WebhookEdit) error {
	endpoint := baseEndpoint + fmt.Sprintf("webhooks/%s/%s/messages/@original", c.clientID, interactionToken)
	buf, err := newBuffer(interaction)
	if err != nil {
		return err
	}
	req, err := c.newRequest(ctx, http.MethodPatch, endpoint, &buf)
	if err != nil {
		return err
	}
	_, err = c.http.Do(req)
	return err
}

// DeleteInteractionResponse deletes an interaction response
func (c *Client) DeleteInteractionResponse(ctx context.Context, interactionToken string) error {
	endpoint := baseEndpoint + fmt.Sprintf("webhooks/%s/%s/messages/@original", c.clientID, interactionToken)
	req, err := c.newRequest(ctx, http.MethodDelete, endpoint, nil)
	if err != nil {
		return err
	}
	resp, err := c.http.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("returned non-200 status code: %s", resp.Status)
	}
	return nil
}

// CreateFollowupMessage creates a followup message
func (c *Client) CreateFollowupMessage(ctx context.Context, interactionToken string, interaction *webhook.Webhook) error {
	endpoint := baseEndpoint + fmt.Sprintf("webhooks/%s/%s", c.clientID, interactionToken)
	buf, err := newBuffer(interaction)
	if err != nil {
		return err
	}
	req, err := c.newRequest(ctx, http.MethodPost, endpoint, &buf)
	if err != nil {
		return err
	}
	_, err = c.http.Do(req)
	return err
}

// EditFollowupMessage edits a followup message
func (c *Client) EditFollowupMessage(ctx context.Context, interactionToken, messageID string, interaction *webhook.WebhookEdit) error {
	endpoint := baseEndpoint + fmt.Sprintf("webhooks/%s/%s/messages/%s", c.clientID, interactionToken, messageID)
	buf, err := newBuffer(interaction)
	if err != nil {
		return err
	}
	req, err := c.newRequest(ctx, http.MethodPatch, endpoint, &buf)
	if err != nil {
		return err
	}
	_, err = c.http.Do(req)
	return err
}

// DeleteFollowupMessage deletes a followup message
func (c *Client) DeleteFollowupMessage(ctx context.Context, interactionToken, messageID string) error {
	endpoint := baseEndpoint + fmt.Sprintf("webhooks/%s/%s/messages/%s", c.clientID, interactionToken, messageID)
	req, err := c.newRequest(ctx, http.MethodPatch, endpoint, nil)
	if err != nil {
		return err
	}
	resp, err := c.http.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("returned non-200 status code: %s", resp.Status)
	}
	return nil
}
